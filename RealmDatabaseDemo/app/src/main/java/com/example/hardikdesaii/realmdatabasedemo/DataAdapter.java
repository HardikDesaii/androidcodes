package com.example.hardikdesaii.realmdatabasedemo;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;

/**
 * Created by HardikDesaii on 16/02/17.
 */

public class DataAdapter extends RealmRecyclerViewAdapter<DataItems, DataAdapter.ViewHolder>
{
    private List<DataItems> mDataItems;
    private Context context;
    private Realm realm;
    private LayoutInflater inflater;

    DataAdapter(Context context, OrderedRealmCollection<DataItems> mDataItems)
{
    super(context, mDataItems, true);
    this.context=context;
    this.mDataItems = mDataItems;
}

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.cardview_realm, null);
        DataAdapter.ViewHolder userViewHolder = new DataAdapter.ViewHolder(view);
        return userViewHolder;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position)
    {
        this.realm = Realm.getDefaultInstance();

        final DataItems item = getItem(position);

            holder.one.setText(item.getOrder_id());
            holder.two.setText(item.getName());
            holder.three.setText(item.getStatus());
            holder.four.setText(item.getDate_added());
            holder.five.setText(String.valueOf(item.getProducts()));
            holder.six.setText(item.getTotal());
            holder.seven.setText(item.getHref());
            holder.update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {

                    inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View content = inflater.inflate(R.layout.update_data, null);

                    final EditText ed2 = (EditText) content.findViewById(R.id.ed2);
                    final EditText ed3 = (EditText) content.findViewById(R.id.ed3);
                    final EditText ed4 = (EditText) content.findViewById(R.id.ed4);
                    final EditText ed5 = (EditText) content.findViewById(R.id.ed5);
                    final EditText ed6 = (EditText) content.findViewById(R.id.ed6);
                    final EditText ed7 = (EditText) content.findViewById(R.id.ed7);

                    ed2.setText(item.getName());
                    ed3.setText(item.getStatus());
                    ed4.setText(item.getDate_added());
                    ed5.setText(String.valueOf(item.getProducts()));
                    ed6.setText(item.getTotal());
                    ed7.setText(item.getHref());
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setView(content)
                            .setTitle("Update student data")
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    RealmResults<DataItems> results = realm.where(DataItems.class).findAll();

                                    realm.beginTransaction();
                                    //  results.get(position).setId(Integer.parseInt(editId.getText().toString()));
                                    results.get(position).setName(ed2.getText().toString());
                                    results.get(position).setStatus(ed3.getText().toString());
                                    results.get(position).setDate_added(ed4.getText().toString());
                                    results.get(position).setProducts(Integer.parseInt(ed5.getText().toString()));
                                    results.get(position).setTotal(ed6.getText().toString());
                                    results.get(position).setHref(ed7.getText().toString());



                                    realm.commitTransaction();

                                    notifyDataSetChanged();
                                }
                            })
                            .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    AlertDialog dialog = builder.create();
                    dialog.show();


                }
            });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RealmResults<DataItems> results = realm.where(DataItems.class).findAll();

                // Get the book title to show it in toast message
                DataItems studentList = results.get(position);


                // All changes to data must happen in a transaction
                realm.beginTransaction();

                // remove single match
                results.deleteFromRealm(position);
                realm.commitTransaction();

                notifyDataSetChanged();
                Toast.makeText(context, "Data at Position :" +(position+1)+ "  is removed from Realm", Toast.LENGTH_SHORT).show();

            }
        });

    }
    @Override
    public int getItemCount()
    {
        return mDataItems.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView one,two,three,four,five,six,seven;
        Button update,delete;

        public ViewHolder(View itemView)
        {
            super(itemView);
            one=(TextView)itemView.findViewById(R.id.jsonarg1);
            two=(TextView)itemView.findViewById(R.id.jsonarg2);
            three=(TextView)itemView.findViewById(R.id.jsonarg3);
            four=(TextView)itemView.findViewById(R.id.jsonarg4);
            five=(TextView)itemView.findViewById(R.id.jsonarg5);
            six=(TextView)itemView.findViewById(R.id.jsonarg6);
            seven=(TextView)itemView.findViewById(R.id.jsonarg7);
            update=(Button)itemView.findViewById(R.id.btnupdate_cardview);
            delete=(Button)itemView.findViewById(R.id.btndelete_cardview);
        }
    }
}
