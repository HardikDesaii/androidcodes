package com.example.hardikdesaii.realmdatabasedemo;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by HardikDesaii on 16/02/17.
 */

public class DataItems extends RealmObject
{
    @PrimaryKey
    private String order_id;

    private String name;
    private String status;
    private String date_added;
    private int products;
    private String total;
    private String href;

  public DataItems()
  {

  }
   DataItems( String order_id, String name, String status, String date_added, int products, String total, String href)

    {
        this.order_id=order_id;
        this.name=name;
        this.status=status;
        this.date_added=date_added;
        this.products=products;
        this.total=total;
        this.href=href;
    }


    public int getProducts() {
        return products;
    }

    public String getDate_added() {
        return date_added;
    }

    public String getHref() {
        return href;
    }

    public String getName() {
        return name;
    }

    public String getOrder_id() {
        return order_id;
    }

    public String getStatus() {
        return status;
    }

    public String getTotal() {
        return total;
    }

    public void setDate_added(String date_added) {
        this.date_added = date_added;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public void setProducts(int products) {
        this.products = products;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}
