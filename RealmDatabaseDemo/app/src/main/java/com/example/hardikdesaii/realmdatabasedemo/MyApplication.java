package com.example.hardikdesaii.realmdatabasedemo;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by HardikDesaii on 16/02/17.
 */

public class MyApplication extends Application
{
    @Override
    public void onCreate() {

        super.onCreate();
        //It controls all objects that how realm is created.

        // new SharedPrefHelper(this);  as we dont need sharedprefrence in out app
        Realm.init(this);
        RealmConfiguration realmConfiguration =
                new RealmConfiguration.Builder().build();
        Realm.setDefaultConfiguration(realmConfiguration);

    }
}
