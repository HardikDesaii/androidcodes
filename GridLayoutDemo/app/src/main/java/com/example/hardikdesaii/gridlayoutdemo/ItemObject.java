package com.example.hardikdesaii.gridlayoutdemo;

/**
 * Created by HardikDesaii on 21/02/17.
 */
public class ItemObject
{
    String name;
    int image;
    ItemObject(String name,int image)
    {
        this.name=name;
        this.image=image;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public int getImage() {
        return image;
    }
}
