package com.example.hardikdesaii.gridlayoutdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
{

    private GridLayoutManager lLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        List<ItemObject> rowListItem = getAllItemList();
        // last parameter indicates number of grids
        lLayout = new GridLayoutManager(MainActivity.this, 2);

        RecyclerView rView = (RecyclerView)findViewById(R.id.Gridrecyclerview);
        rView.setHasFixedSize(true);
        rView.setLayoutManager(lLayout);

        RecyclerViewAdapter rcAdapter = new RecyclerViewAdapter(MainActivity.this, rowListItem);
        rView.setAdapter(rcAdapter);

    }
    private List<ItemObject> getAllItemList(){

        List<ItemObject> allItems = new ArrayList<ItemObject>();
        allItems.add(new ItemObject("United States", R.mipmap.ic_launcher));
        allItems.add(new ItemObject("Canada", R.mipmap.ic_launcher));
        allItems.add(new ItemObject("United Kingdom", R.mipmap.ic_launcher));
        allItems.add(new ItemObject("Germany",R.mipmap.ic_launcher));
        allItems.add(new ItemObject("Sweden", R.mipmap.ic_launcher));
        allItems.add(new ItemObject("United Kingdom",R.mipmap.ic_launcher));
        allItems.add(new ItemObject("Germany",R.mipmap.ic_launcher));
        allItems.add(new ItemObject("Sweden", R.mipmap.ic_launcher));
        allItems.add(new ItemObject("United States",R.mipmap.ic_launcher));
        allItems.add(new ItemObject("Canada",R.mipmap.ic_launcher));
        allItems.add(new ItemObject("United Kingdom",R.mipmap.ic_launcher));
        allItems.add(new ItemObject("Germany",R.mipmap.ic_launcher));
        allItems.add(new ItemObject("Sweden",R.mipmap.ic_launcher));
        allItems.add(new ItemObject("United Kingdom",R.mipmap.ic_launcher));
        allItems.add(new ItemObject("Germany",R.mipmap.ic_launcher));
        allItems.add(new ItemObject("Sweden",R.mipmap.ic_launcher));

        return allItems;
    }
}
