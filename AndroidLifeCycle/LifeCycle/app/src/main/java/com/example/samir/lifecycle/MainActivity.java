package com.example.samir.lifecycle;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        Log.e("onStart","called");
    }

    @Override
    protected void onRestart() {

        Log.e("onRestart","called");
    }

    @Override
    protected void onResume() {

        Log.e("onResume","called");
    }

    @Override
    protected void onStop() {

        Log.e("onStop","called");
    }

    @Override
    protected void onDestroy() {

        Log.e("onDestroy","called");
    }
}
